BIN     = textbox
DEST    = /usr/local
MANDEST = $(DEST)/share/man

CC      = gcc
CFLAGS  = -Wall -O2 -march=native -pipe

OBJS    = textbox.o

%.o: %.c
	$(CC) $(CFLAGS) -c $<

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ 

install:
	mkdir -p $(DEST)/bin
	cp -f $(BIN) $(DEST)/bin
	chmod 755 $(DEST)/bin/$(BIN)
	mkdir -p $(MANDEST)/man1
	cp -f $(BIN).1 $(MANDEST)/man1
	chmod 644 $(MANDEST)/man1/$(BIN).1

uninstall:
	rm -f $(DEST)/bin/$(BIN) $(MANDEST)/man1/$(BIN).1

clean:
	rm -f *.o $(BIN)

.PHONY: install uninstall clean
