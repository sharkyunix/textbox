#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#define VERSION "0.1.1"

/* counts the number of characters */
int countChars(char **text, int argc)
{
	int i, j, nc = 0;
	for (i = 1; i < argc; i++)
		if (text[i][0] == '-')
			continue;
		else
			for (j = 0; j < text[i][j]; j++)
				nc++;
	return nc;
}

void help()
{
	puts("Usage:");
	puts("textbox [options] [text]...\n");
	puts("Options:");
	puts("-v, show version");
	puts("-h, show this menu");
	puts("-d, print alternative box");
	puts("-l, print text in lowercase");
}

void printSeparator(int argc, int alt, int nc, int compensator)
{
	int i;

	/* number of argcs minus name of program and options + two spaces */
	compensator = argc - compensator + 2;
	/*  print alternative box, top and bottom of the box */
	if (alt) {
		printf("+");
		for (i = 0; i <= nc + compensator - 2; i++)
			printf("-");
		printf("+\n");
	/* print default box, top and bottom of the box */
	} else {
		for (i = 0; i <= nc + compensator; i++)
			printf("#");
		puts("");
	}
}

/* prints the "wall" character in the beginning line containing text */
void printWallBeginning(int alt)
{
	if (alt)
		printf("| ");
	else
		printf("# ");
}

/* prints the "wall" character in the end line containing text */
void printWallEnd(int alt)
{
	if (alt)
		printf("|\n");
	else
		printf("#\n");
}

void printText(char **text, int argc, int nc, int alt, int upper, int compensator)
{
	int i, j;

	/* print text */
	for (i = 1; i < argc; i++) {
		if (text[i][0] == '-')
			continue;
		else
			for (j = 0; j < text[i][j]; j++)
				if (text[i][j] >= 'a' && text[i][j] <= 'z' && upper)
					printf("%c", text[i][j] - 32);
				else
					printf("%c", text[i][j]);
		printf(" ");
	}
}

int main(int argc, char **argv)
{
	int i, number_chars, alt = 0, upper = 1, compensator = 1;

	/* change the language to the operating systeem standard */
	setlocale(LC_ALL, "");

	/* get options */
	for (i = 1; i < argc; i++)
		/* these options take no arguments */
		if (!strcmp("-h", argv[i])){
			help();
			exit(0);
		}
		else if (!strcmp("-v", argv[i])) {
			puts("textbox-"VERSION);
			exit(0);
		}

		/* these options take multiples arguments */
		else if (!strcmp("-d", argv[i])) {
			alt = 1;
			compensator++;
		} else if (!strcmp("-l", argv[i])) {
			upper = 0;
			compensator++;
		/* options error handling */
		} else if (argv[i][0] == '-') {
			fprintf(stderr, "-%c is am invalid option! use -h to see help menu\n", argv[i][1]);
			exit(1);
		}

	if (argc == 1) {
		help();
		exit(0);
	} else {
		number_chars = countChars(argv, argc);
		printSeparator(argc, alt, number_chars, compensator);
		printWallBeginning(alt);
		printText(argv, argc, number_chars, alt, upper, compensator);
		printWallEnd(alt);
		printSeparator(argc, alt, number_chars, compensator);
		exit(0);
	}

	/* unreachable */	
	fputs("Something goes very wrong", stderr);
	return 1;
}
